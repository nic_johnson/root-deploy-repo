# root-deploy-repo

Pretend that `dev.env` & `stg.env` are never actually committed. Just commited them for
demonstration purposes

This results in

```
❯ kc get gitrepo -n flux-system
NAME               URL                                               AGE     READY   STATUS
cluster-repo       https://gitlab.com/nic_johnson/root-deploy-repo   5m12s   True    stored artifact for revision 'main/812401c84002bff207e7b2af65192a62ef8512a0'
service-repo-dev   https://gitlab.com/nic_johnson/service-repo       4m34s   True    stored artifact for revision 'v1.1.0/cb6805b2d1debd0da36861bc8c851711dfcb2d76'
service-repo-stg   https://gitlab.com/nic_johnson/service-repo       4m34s   True    stored artifact for revision 'v1.0.0/006b455e37ce7aca881ce9e2425a16d5796d1f61'
                                                                                                                                                                                                                                                                                                                             
❯ kc get kustomization -n flux-system
NAME               AGE     READY   STATUS
cluster-repo       5m15s   True    Applied revision: main/812401c84002bff207e7b2af65192a62ef8512a0
service-repo-dev   4m37s   True    Applied revision: v1.1.0/cb6805b2d1debd0da36861bc8c851711dfcb2d76
service-repo-stg   4m37s   True    Applied revision: v1.0.0/006b455e37ce7aca881ce9e2425a16d5796d1f61
                                                                                                                                                                                                                                                                                                                             
❯ kc get ns
NAME              STATUS   AGE
default           Active   7m5s
flux-system       Active   6m40s
kube-node-lease   Active   7m7s
kube-public       Active   7m7s
kube-system       Active   7m7s
pf-dev            Active   94s
pf-stg            Active   90s
sealed-secret     Active   4m58s
                                                                                                                                                                                                                                                                                                                             
❯ kc get configmap -n pf-dev service-config  -o yaml
apiVersion: v1
data:
  foo-config: bar
  some-url: https://dev.place.com
kind: ConfigMap
metadata:
  creationTimestamp: "2023-02-06T17:04:38Z"
  labels:
    kustomize.toolkit.fluxcd.io/name: service-repo-dev
    kustomize.toolkit.fluxcd.io/namespace: flux-system
  name: service-config
  namespace: pf-dev
  resourceVersion: "1224"
  uid: 1ec03ba5-61f5-4ace-a694-dd633f603fce
                                                                                                                                                                                                                                                                                                                             
❯ kc get configmap -n pf-stg service-config  -o yaml
apiVersion: v1
data:
  some-url: https://stg.place.com
kind: ConfigMap
metadata:
  creationTimestamp: "2023-02-06T17:04:42Z"
  labels:
    kustomize.toolkit.fluxcd.io/name: service-repo-stg
    kustomize.toolkit.fluxcd.io/namespace: flux-system
  name: service-config
  namespace: pf-stg
  resourceVersion: "1243"
  uid: 2c969fdc-ba5c-454a-a5ba-355e5d550be7

```
